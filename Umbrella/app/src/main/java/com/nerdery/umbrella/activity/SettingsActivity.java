package com.nerdery.umbrella.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nerdery.umbrella.AppController;
import com.nerdery.umbrella.R;


public class SettingsActivity extends AppCompatActivity {
    private LinearLayout zipLayout;
    private TextView zipTextview;
    private LinearLayout unitsLayout;
    private TextView unitsTextview;
    private AppController appController = AppController.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set color of actionbar
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat
                .getColor(getApplicationContext(), R.color.settings_actionbar)));
        getSupportActionBar().setTitle(getResources().getString(R.string.actionbar_settings_title));

        // set up zip UI
        zipLayout = (LinearLayout) findViewById(R.id.zipcode_linearlayout);
        zipTextview = (TextView) findViewById(R.id.zip_textview);
        zipTextview.setText(appController.getCurZip().toString());
        zipLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zipAlertDialog();
            }
        });

        // set up units UI
        unitsLayout = (LinearLayout) findViewById(R.id.units_linearlayout);
        unitsTextview = (TextView) findViewById(R.id.units_textview);
        unitsTextview.setText(appController.getUseFahrenheit() ? R.string.fahrenheit : R.string.celsius);
        unitsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unitsAlertDialog();
            }
        });
    }

    /*
     * sets up an alert dialog with options to select units
     */
    private void unitsAlertDialog() {
        final CharSequence units[] = new CharSequence[]
                {getResources().getString(R.string.fahrenheit),
                        getResources().getString(R.string.celsius)};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Unit");
        builder.setItems(units, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                unitsTextview.setText(units[which]);
                // it's binary, we just need to remember that fahrenheight is 0
                // not expandable if we add other units later on, but that's also unlikely
                appController.setUseFahrenheit(which == 0);
            }
        });
        builder.show();
    }

    /*
     * sets up an alert dialog with input to enter zip
     */
    private void zipAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Enter ZIP");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                changeZip(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    /*
    * change teh ZIP both in the UI and in the app settings
    */
    private void changeZip(String s) {
        Integer zip = Integer.parseInt(s);
        if(s.length() == 5) {
            appController.setCurZip(zip);
            zipTextview.setText(s);
        }
        else{
            appController.toast(getResources().getString(R.string.error_invalid_zip));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_settings, menu);
        return true;
    }

}
