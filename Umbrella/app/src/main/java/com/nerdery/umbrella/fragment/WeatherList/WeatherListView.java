package com.nerdery.umbrella.fragment.WeatherList;

import com.nerdery.umbrella.model.ForecastCondition;
import com.nerdery.umbrella.model.WeatherData;

import java.util.List;
import java.util.SortedMap;

/**
 * Created by jayd on 12/27/15.
 */
public interface  WeatherListView {
    public void setData(WeatherData weatherData, SortedMap<Integer, List<ForecastCondition>> integerListSortedMap);
    public void setActionbarContent(WeatherData weatherData);
}
