package com.nerdery.umbrella.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nerdery.umbrella.R;

/**
 * Created by jayd on 12/26/15.
 */
public class HourlyForecastHolder extends RecyclerView.ViewHolder {
    public TextView hourText;
    public TextView tempText;
    public ImageView conditionImage;

    public HourlyForecastHolder(View itemView) {
        super(itemView);
        hourText = (TextView) itemView.findViewById(R.id.hour_textview);
        tempText = (TextView) itemView.findViewById(R.id.temp_textview);
        conditionImage = (ImageView) itemView.findViewById(R.id.condition_imageview);
    }
}
