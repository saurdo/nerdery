package com.nerdery.umbrella.holder;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.nerdery.umbrella.R;

/**
 * Created by jayd on 12/26/15.
 */
public class ForecastHolder extends RecyclerView.ViewHolder {
    public TextView dayText;
    public CardView cardView;
    public RecyclerView hourlyforecastRecyclerView;


    public ForecastHolder(View itemView) {
        super(itemView);
        cardView = (CardView) itemView.findViewById(R.id.card_view);
        dayText = (TextView) itemView.findViewById(R.id.day_textview);
        hourlyforecastRecyclerView = (RecyclerView) itemView.findViewById(R.id.hourly_forecast_recycler_view);
    }
}
