package com.nerdery.umbrella;

import android.app.Application;
import android.content.res.Resources;
import android.support.v4.app.AppOpsManagerCompat23;
import android.widget.Toast;

/**
 * Created by jayd on 12/26/15.
 */
public class AppController extends Application {
    private static AppController ourInstance;
    public static AppController getInstance() {
        return ourInstance;
    }

    // you know, the circle thing
    public static final char degSymbol = (char) 0x00B0;

    public Integer getCurZip() {
        return curZip;
    }

    public void setCurZip(Integer curZip) {
        this.curZip = curZip;
    }
    private Integer curZip;
    public Boolean getUseFahrenheit() {
        return useFahrenheit;
    }

    public void setUseFahrenheit(Boolean useFahrenheit) {
        this.useFahrenheit = useFahrenheit;
    }

    private Boolean useFahrenheit = true;

    @Override
    public void onCreate(){
        super.onCreate();
        ourInstance = this;
        curZip = getResources().getInteger(R.integer.default_zip);
    }

    public void toast(String s) {
        Toast.makeText(getApplicationContext(), s, Toast.LENGTH_LONG).show();
    }
}
