package com.nerdery.umbrella.fragment.WeatherList;


import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nerdery.umbrella.AppController;
import com.nerdery.umbrella.R;
import com.nerdery.umbrella.activity.SettingsActivity;
import com.nerdery.umbrella.adapter.ForecastAdapter;
import com.nerdery.umbrella.model.CurrentObservation;
import com.nerdery.umbrella.model.ForecastCondition;
import com.nerdery.umbrella.model.WeatherData;
import com.nerdery.umbrella.presenter.WeatherListPresenter;

import java.util.List;
import java.util.SortedMap;

public class WeatherListFragment extends Fragment implements WeatherListView {
    private RecyclerView forecastRecyclerView;
    private LinearLayout currentConditionsBar;
    private AppController appController = AppController.getInstance();
    private WeatherListPresenter weatherListPresenter;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_weather_list, container, false);
        forecastRecyclerView = (RecyclerView) view.findViewById(R.id.forecast_recycler_view);
        forecastRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        currentConditionsBar = (LinearLayout) view.findViewById(R.id.current_conditions_bar);
        weatherListPresenter = new WeatherListPresenter(this);

        return view;
    }


    @Override
    public void onStart(){
        super.onStart();
        weatherListPresenter.onResume();
    }

    /*
    * Seems kind of terrible to manipulate an activity from a fragment. We'll see if I can come up with something better
    */
    public void setActionbarContent(WeatherData weatherData) {
        // get actionbar from main activity
        android.support.v7.app.ActionBar bar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        CurrentObservation curObs = weatherData.currentObservation;

        // update UI
        Integer bgColor = curObs.tempFahrenheit >= 60 ? R.color.weather_warm : R.color.weather_cool;
        // update color
        bgColor = ContextCompat.getColor(getActivity(), bgColor);
        currentConditionsBar.setBackgroundColor(bgColor);
        bar.setBackgroundDrawable(new ColorDrawable(bgColor));

        // which unit
        float temp = appController.getUseFahrenheit() ? curObs.tempFahrenheit : curObs.tempCelsius;
        ((TextView) currentConditionsBar.findViewById(R.id.cur_temp_textview)).setText(String.format("%.0f%c", temp, appController.degSymbol));

        ((TextView) currentConditionsBar.findViewById(R.id.cur_cond_textview)).setText(curObs.weather);
        bar.setTitle(String.format("%s, %s", curObs.displayLocation.city, curObs.displayLocation.state));


    }

    @Override
    public void setData(WeatherData weatherData, SortedMap<Integer, List<ForecastCondition>> forecastConditionByDay) {
        setActionbarContent(weatherData);
        forecastRecyclerView.setAdapter(new ForecastAdapter(getActivity(), forecastConditionByDay));
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        // remove reference to presenter and from presenter
        weatherListPresenter.takeView(null);
        weatherListPresenter = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_item_settings) {
            Intent browserIntent = new Intent(getActivity(), SettingsActivity.class);
            startActivity(browserIntent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
