package com.nerdery.umbrella.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.nerdery.umbrella.R;
import com.nerdery.umbrella.fragment.WeatherList.WeatherListFragment;

public class WeatherListActivity extends AppCompatActivity {
    private android.support.v7.app.ActionBar bar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_list);
        // remove shadow in actionbar so it doesn't show up on the fragment
        bar = getSupportActionBar();
        bar.setElevation(0);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment == null) {
            fragment = new WeatherListFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container, fragment)
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.actionbar_weather_list, menu);
        return true;
    }


}
