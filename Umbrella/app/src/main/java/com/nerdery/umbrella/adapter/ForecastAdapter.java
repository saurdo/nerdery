package com.nerdery.umbrella.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nerdery.umbrella.R;
import com.nerdery.umbrella.holder.ForecastHolder;
import com.nerdery.umbrella.model.ForecastCondition;
import com.nerdery.umbrella.widget.DynamicGridLayoutManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.SortedMap;

/**
 * Created by jayd on 12/26/15.
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastHolder>{
    private Activity activity;
    private List<Integer> forecastConditionDays;
    private SortedMap<Integer, List<ForecastCondition>> forecastConditionByDay;

    public ForecastAdapter(Activity activity, SortedMap<Integer, List<ForecastCondition>> forecastConditionByDay){
        this.activity = activity;
        this.forecastConditionByDay = forecastConditionByDay;
        // get list of days from keys, we'll use this as our list
        forecastConditionDays = new ArrayList<>(forecastConditionByDay.keySet());
    }

    @Override
    public ForecastHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater
                .inflate(R.layout.list_item_forecast, parent, false);
        return new ForecastHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastHolder holder, int position) {
        String day;
        // set title depending on day (this assumes the smallest day of year number is the current day
        if(position == 0){
            day = activity.getResources().getString(R.string.today);
        }
        else if(position == 1){
            day = activity.getResources().getString(R.string.tomorrow);
        }
        else {
            Calendar cal = new GregorianCalendar();
            Integer d = forecastConditionDays.get(position);
            cal.setTime(forecastConditionByDay.get(d).get(0).time);
            day = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.US);
        }
        // update UI
        holder.dayText.setText(day);
        holder.hourlyforecastRecyclerView.setLayoutManager(new DynamicGridLayoutManager(activity, 4));
        Integer key = forecastConditionDays.get(position);
        holder.hourlyforecastRecyclerView.setAdapter(new HourlyForecastAdapter(activity, forecastConditionByDay.get(key)));

    }

    @Override
    public int getItemCount() {
        return forecastConditionByDay.keySet().size();
    }
}
