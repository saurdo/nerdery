package com.nerdery.umbrella.presenter;

import android.util.Log;

import com.nerdery.umbrella.AppController;
import com.nerdery.umbrella.R;
import com.nerdery.umbrella.fragment.WeatherList.WeatherListView;
import com.nerdery.umbrella.api.ApiManager;
import com.nerdery.umbrella.model.ForecastCondition;
import com.nerdery.umbrella.model.WeatherData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.SortedMap;
import java.util.TimeZone;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jayd on 12/27/15.
 */
public class WeatherListPresenter {
    private static final String TAG = "WeatherListPresenter";

    private WeatherListView weatherListView;
    private AppController appController = AppController.getInstance();

    public WeatherListPresenter(WeatherListView weatherListView) {
        this.weatherListView = weatherListView;
    }

    public void takeView(WeatherListView weatherListView){
        this.weatherListView = weatherListView;
    }

    public void onResume() {

        // query with currently set ZIP
        ApiManager.getWeatherApi().getForecastForZip(appController.getCurZip(), new Callback<WeatherData>() {
            @Override
            public void success(WeatherData weatherData, Response response) {
                // if the API returned an error, this will catch it
                // Also it will catch if our view disappeared
                if (weatherData != null && weatherData.forecast != null && weatherListView != null) {
                    weatherListView.setData(weatherData, organizeForecastData(weatherData));
                }
                else{
                    //likely due to a bad zip.
                    appController.toast(appController.getApplicationContext().getResources().getString(R.string.error_bad_zip));
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }

    /*
     * Organize forecast data by day
     */
    private SortedMap<Integer, List<ForecastCondition>> organizeForecastData(WeatherData weatherData) {
        SortedMap<Integer, List<ForecastCondition>> forecastConditionByDay = new TreeMap<>();

        for(ForecastCondition forecastCondition : weatherData.forecast){
            Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(weatherData.currentObservation.timezone));
            // adjust for timezone
            cal.setTime(forecastCondition.time);
            Integer key = cal.get(Calendar.DAY_OF_YEAR);
            if(!forecastConditionByDay.containsKey(key)){
                forecastConditionByDay.put(key, new ArrayList<ForecastCondition>());
            }
            forecastConditionByDay.get(key).add(forecastCondition);
        }

        //now let's sort them all by hour... just in case
        for(Integer key : forecastConditionByDay.keySet()){
            Collections.sort(forecastConditionByDay.get(key), new Comparator<ForecastCondition>() {
                @Override
                public int compare(ForecastCondition lhs, ForecastCondition rhs) {
                    return lhs.time.compareTo(rhs.time);
                }
            });
        }

        return forecastConditionByDay;
    }
}
