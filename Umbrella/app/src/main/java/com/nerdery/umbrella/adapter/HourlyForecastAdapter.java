package com.nerdery.umbrella.adapter;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nerdery.umbrella.AppController;
import com.nerdery.umbrella.R;
import com.nerdery.umbrella.holder.HourlyForecastHolder;
import com.nerdery.umbrella.api.IconApi;
import com.nerdery.umbrella.model.ForecastCondition;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by jayd on 12/26/15.
 */
public class HourlyForecastAdapter extends RecyclerView.Adapter<HourlyForecastHolder>{
    private AppController appController = AppController.getInstance();
    private Activity activity;
    private List<ForecastCondition> forecasts;
    private IconApi iconApi;
    // for highlighting
    private int hottestPos;
    private int coolestPos;

    public HourlyForecastAdapter(Activity activity, List<ForecastCondition> forecasts){
        this.activity = activity;
        this.forecasts = forecasts;
        this.iconApi = new IconApi();
        hottestPos= 0;
        coolestPos = 0;
        findHottestAndCoolest();
    }

    /*
     * Find hottest and coldest temps from list
     */
    private void findHottestAndCoolest() {
        float hottest = forecasts.get(0).tempFahrenheit;
        float coolest = forecasts.get(0).tempFahrenheit;
        for(int i = 0; i < forecasts.size(); i++){
            if(forecasts.get(i).tempFahrenheit < coolest){
                coolest = forecasts.get(i).tempFahrenheit;
                coolestPos = i;
            }
            if(forecasts.get(i).tempFahrenheit > hottest){
                hottest = forecasts.get(i).tempFahrenheit;
                hottestPos = i;
            }
        }
    }

    @Override
    public HourlyForecastHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(activity);
        View view = layoutInflater
                .inflate(R.layout.list_item_hourly_forecast, parent, false);
        return new HourlyForecastHolder(view);
    }

    @Override
    public void onBindViewHolder(HourlyForecastHolder holder, int position) {
        ForecastCondition curForecast = forecasts.get(position);

        holder.hourText.setText(curForecast.displayTime);

        //figure out what unit to use
        float temp = appController.getUseFahrenheit() ? curForecast.tempFahrenheit :
                curForecast.tempCelsius;
        holder.tempText.setText(String.format("%.0f%c", temp, AppController.degSymbol));

        // retrieve icon
        String icon = iconApi.getUrlForIcon(curForecast.icon, false);
        Picasso.with(activity).load(icon).into(holder.conditionImage);

        // update color of cell
        Integer weatherColor = R.color.weather_default;
        // don't tint if hottest and coolest are in the same hour
        if(hottestPos != coolestPos) {
            if (position == hottestPos) weatherColor = R.color.weather_warm;
            else if (position == coolestPos) weatherColor = R.color.weather_cool;
        }
        weatherColor = ContextCompat.getColor(activity, weatherColor);

        // set colors
        holder.conditionImage.setColorFilter(
                new PorterDuffColorFilter(weatherColor, PorterDuff.Mode.SRC_IN));
        holder.hourText.setTextColor(weatherColor);
        holder.tempText.setTextColor(weatherColor);
    }

    @Override
    public int getItemCount() {
        return forecasts.size();
    }
}
